## Java Spring Boot invoices api

Tested with Java 8 and maven.

Run tests with

    mvn test

Build & run with

    mvn install
    java -jar target/invoices-0.0.1-SNAPSHOT.jar

h2 console is located at http://localhost:8080/h2 with admin/admin.
Swagger docs are at http://localhost:8080/swagger-ui.html