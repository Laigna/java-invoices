package com.tommiindustries.invoices.api;

import com.tommiindustries.invoices.services.InvoicesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController()
@RequestMapping(value = "/api/invoices", produces = "application/json")
public class InvoicesRestController {

    @Autowired
    private InvoicesService invoicesService;

    @RequestMapping(value = "/{id}/isPaid", method = GET)
    public ResponseEntity getIsInvoicePaid(@PathVariable("id") String id) {
        try {
            Boolean isPaid = invoicesService.getIsInvoicePaid(id);
            return ResponseEntity.ok(new SuccessResponse().setPaid(isPaid));
        } catch (IllegalArgumentException ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new ErrorResponse().setMessage(ex.getMessage()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new ErrorResponse().setMessage(ex.getMessage()));
        }
    }

    private class SuccessResponse {

        private Boolean isPaid;

        public Boolean getPaid() {
            return isPaid;
        }

        public SuccessResponse setPaid(Boolean paid) {
            this.isPaid = paid;
            return this;
        }
    }

    private class ErrorResponse {

        private String message;

        public String getMessage() {
            return message;
        }

        public ErrorResponse setMessage(String message) {
            this.message = message;
            return this;
        }
    }
}
