package com.tommiindustries.invoices.services;

import com.google.common.base.Charsets;
import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;
import com.tommiindustries.invoices.models.InvoicePo;
import com.tommiindustries.invoices.models.InvoicesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BloomConfig {

    /**
     * The bitsize of the bloom filter is calculated by
     * m = - n ln(p) / (ln(2))^2 ~ - 2.08 n ln(p)
     * where p is the false positives rate and n is the number of insertions.
     *
     * n = 500 000 000, p = 5% gives a filter size of 371.65MB and 4 hashes.
     */
    private final static long BLOOM_FILTER_EXPECTED_INSERTIONS = 500 * 1000 * 1000;
    private final static double BLOOM_FILTER_DESIRED_ERROR_RATE = 0.05;

    @Autowired
    private InvoicesRepository invoicesRepository;

    @Bean
    public BloomFilter<String> invoicesBloomFilter() {
        BloomFilter bloomFilter = BloomFilter.create(
                Funnels.stringFunnel(Charsets.UTF_8),
                BLOOM_FILTER_EXPECTED_INSERTIONS,
                BLOOM_FILTER_DESIRED_ERROR_RATE);
        for (InvoicePo invoicePo : invoicesRepository.findAll()) {
            bloomFilter.put(invoicePo.getId());
        }
        return bloomFilter;
    }
}
