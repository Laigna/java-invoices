package com.tommiindustries.invoices.services;

import com.google.common.hash.BloomFilter;
import com.tommiindustries.invoices.models.InvoicePo;
import com.tommiindustries.invoices.models.InvoicesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Transactional
@Service
public class InvoicesService {

    @Autowired
    private InvoicesRepository invoicesRepository;

    @Autowired
    private BloomFilter<String> bloomFilter;

    public Boolean getIsInvoicePaid(String id) {
        if (id == null
                || id.length() < InvoicePo.ID_MIN_LENGTH
                || id.length() > InvoicePo.ID_MAX_LENGTH
                || !id.chars().allMatch(Character::isDigit)) {
            throw new IllegalArgumentException("Id must be a numerical value with 10-25 digits.");
        }
        if (bloomFilter.mightContain(id)) {
            Optional<InvoicePo> maybeInvoice = invoicesRepository.findById(id);
            return maybeInvoice.isPresent() ? maybeInvoice.get().getPaid() : false;
        } else {
            return false;
        }
    }
}
