package com.tommiindustries.invoices.models;

import javax.persistence.*;

/**
 * Persistence object for an invoice.
 */
@Entity
@Table(name = "INVOICES")
public class InvoicePo {

    @Transient
    public static final int ID_MIN_LENGTH = 10;

    @Transient
    public static final int ID_MAX_LENGTH = 25;

    @Id
    /**
     * Todo: Proper value generation...
     */
    private String id;

    private Boolean isPaid;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getPaid() {
        return isPaid;
    }

    public void setPaid(Boolean paid) {
        this.isPaid = paid;
    }
}
