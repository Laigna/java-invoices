package com.tommiindustries.invoices.models;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

import static javax.transaction.Transactional.TxType.MANDATORY;

/**
 * Data access object for invoices.
 */
@Transactional(MANDATORY)
@Repository
public interface InvoicesRepository extends CrudRepository<InvoicePo, String> {

}
