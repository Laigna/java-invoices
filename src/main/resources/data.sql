
drop table invoices;
create table invoices
(
  -- id is constrained to be a number with 10-25 digits.
  id varchar(25) default '0000000000' check (char_length(id) >= 10 and id::numeric >= 0),
  is_paid bool not null,
  primary key (id)
);

-- Fill sample data.
insert into invoices values
    ('0000000001', true),
    ('0000000123', false),
    ('0000000456', true),
    ('9999999999999999999999999', true);
