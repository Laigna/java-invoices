package com.tommiindustries.invoices;

import com.tommiindustries.invoices.models.InvoicePo;
import com.tommiindustries.invoices.models.InvoicesRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@RunWith(SpringRunner.class)
@SpringBootTest
public class InvoicesRepositoryTest {

    @Autowired
    private InvoicesRepository invoicesRepository;

    private static final String ID_GOOD = "1234567890";
    private static final String ID_TOO_LONG = "123456789012345678901234567890";
    private static final String ID_TOO_SHORT = "12";
    private static final String ID_NOT_NUMERIC = "1234567890abcdef";

    @Test
    public void checkInvoicesTableConstraints_Id() {
        assertThat(invoicesRepository.findById(ID_GOOD)).isEqualTo(Optional.empty());

        InvoicePo invoicePo = new InvoicePo();
        invoicePo.setId(ID_TOO_SHORT);
        invoicePo.setPaid(false);
        assertThatThrownBy(() -> invoicesRepository.save(invoicePo)).isInstanceOf(DataIntegrityViolationException.class);

        invoicePo.setId(ID_TOO_LONG);
        assertThatThrownBy(() -> invoicesRepository.save(invoicePo)).isInstanceOf(DataIntegrityViolationException.class);

        invoicePo.setId(ID_NOT_NUMERIC);
        assertThatThrownBy(() -> invoicesRepository.save(invoicePo)).isInstanceOf(DataIntegrityViolationException.class);

        invoicePo.setId(ID_GOOD);
        invoicesRepository.save(invoicePo);
        assertThat(invoicesRepository.findById(ID_GOOD).get().getId()).isEqualTo(ID_GOOD);
        invoicesRepository.delete(invoicePo);
    }

    @Test
    public void checkInvoicesTableConstraints_IsPaid() {
        assertThat(invoicesRepository.findById(ID_GOOD)).isEqualTo(Optional.empty());

        InvoicePo invoicePo = new InvoicePo();
        invoicePo.setId(ID_GOOD);
        assertThatThrownBy(() -> invoicesRepository.save(invoicePo)).isInstanceOf(DataIntegrityViolationException.class);

        invoicePo.setPaid(false);
        invoicesRepository.save(invoicePo);
        assertThat(invoicesRepository.findById(ID_GOOD).get().getId()).isEqualTo(ID_GOOD);
        invoicesRepository.delete(invoicePo);
    }
}
