package com.tommiindustries.invoices;

import com.tommiindustries.invoices.services.InvoicesService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@RunWith(SpringRunner.class)
@SpringBootTest
public class InvoicesServiceTest {
    /**
     * See resources/data.sql in the main package for the sample data.
     */

    @Autowired
    private InvoicesService invoicesService;

    @Test
    public void checkIllegalArguments() {
        assertThatThrownBy(() -> invoicesService.getIsInvoicePaid(null)).isInstanceOf(IllegalArgumentException.class);
        assertThatThrownBy(() -> invoicesService.getIsInvoicePaid("")).isInstanceOf(IllegalArgumentException.class);
        assertThatThrownBy(() -> invoicesService.getIsInvoicePaid("ajsidojasoi")).isInstanceOf(IllegalArgumentException.class);
        assertThatThrownBy(() -> invoicesService.getIsInvoicePaid("123")).isInstanceOf(IllegalArgumentException.class);
        assertThatThrownBy(() -> invoicesService.getIsInvoicePaid("-1234567890")).isInstanceOf(IllegalArgumentException.class);
        assertThatThrownBy(() -> invoicesService.getIsInvoicePaid(
                "00000004564120000000456412410000000400000" +
                "00456412000000045641241000000040000000456" +
                "41200000004564124141200410000000400000004")).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void checkServiceValidValuesExistInDb() {
        assertThat(invoicesService.getIsInvoicePaid("0000000001")).isTrue();
        assertThat(invoicesService.getIsInvoicePaid("0000000123")).isFalse();
        assertThat(invoicesService.getIsInvoicePaid("0000000456")).isTrue();
        assertThat(invoicesService.getIsInvoicePaid("9999999999999999999999999")).isTrue();
    }

    @Test
    public void checkServiceValidValuesNotInDb() {
        assertThat(invoicesService.getIsInvoicePaid("000000045641241")).isFalse();
    }
}
